/**
 * Created by Sirormy on 16/1/28.
 */
(function () {
  var $opArea = $('#op-area')
  var sourceUrl = location.href
  function jump (url) {

  }

  $('.menu-item').on('click', function () {
    var $self = $(this)
    var url = $self.data('url')
    jump(url)
    location.hash = url
  })

  // 文章列表
  $('#itemArticleList').on('click', function () {
    $.get('tpl/articleList.html', function (data) {
      $opArea.html(data)
      new Vue({
        el : '#op-area',
        data : {
          article : data_articleList
        }
      })
    })
  })

  // 添加文章
  $('#itemArticleAdd').on('click', function () {
    $.get('tpl/articleAdd.html', function (data) {
      $opArea.html(data)
			K.create('#editor', {
				items : [
				'source', 'undo', 'redo','|', 'preview', 'code', 'cut', 'copy', 'paste',
					'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
					'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
					'superscript', 'clearhtml', 'quickformat', '|', 'fullscreen', '/',
					'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
					'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
					'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons', 'baidumap',
					'anchor', 'link'
				]
			})
    })
  })

  // 用户列表
  $('#itemUserList').on('click', function () {
    $.get('tpl/userList.html', function (data) {
      $opArea.html(data)
      $('#userList').mmGrid({
        cols : gridCols,
        url : "assets/src/userList.json",
        method: "get",
        root: "items",
        height: "500px",
        plugins : [
          $('#userPager').mmPaginator()
        ]
      })
    })
  })

}())