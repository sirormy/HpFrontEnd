/**
 * Created by Sirormy on 16/1/28.
 */
// 文章列表
var data_articleList = [{
	id: 1,
	title: '第一条标题',
	desc: '昨天微博热搜榜第一名是...'
}, {
	id: 2,
	title: '第二条标题',
	desc: '昨天微博热搜榜第二名是...'
}, {
	id: 3,
	title: '第三条标题',
	desc: '昨天微博热搜榜第三名是...'
}, {
	id: 4,
	title: '第四条标题',
	desc: '昨天微博热搜榜第四名是...'
}]

// 位置数据
var locationNodes = {}

// grid数据
var gridCols = [{
	"title" : "id",
	"name" : "id"
}, {
	"title" : "名字",
	"name" : "name"
}, {
	"title" : "年龄",
	"name" : "age"
}]